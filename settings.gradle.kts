include("common", "domain", "repository", "wsclient", "service", "web", "batch", "liquibase", "client", "stub")

rootProject.name = "sbat"

project(":liquibase").projectDir = file("database/liquibase")
project(":common").projectDir = file("application/common")
project(":domain").projectDir = file("application/domain")
project(":repository").projectDir = file("application/repository")
project(":wsclient").projectDir = file("application/wsclient")
project(":service").projectDir = file("application/service")
project(":web").projectDir = file("application/web")
project(":batch").projectDir = file("application/batch")
project(":client").projectDir = file("serviceclient/client")
project(":stub").projectDir = file("serviceclient/stub")

enableFeaturePreview("IMPROVED_POM_SUPPORT")
enableFeaturePreview("STABLE_PUBLISHING")
