import org.gradle.api.file.SourceDirectorySet
import org.liquibase.gradle.LiquibaseExtension
import org.liquibase.gradle.LiquibaseTask
import org.liquibase.groovy.liquibase

apply {
    plugin("org.liquibase.gradle")
}

java.sourceSets["main"].resources {
    srcDir("src/")
}

dependencies {
    runtimeOnly("org.liquibase:liquibase-core")
    runtimeOnly("com.mattbertolini:liquibase-slf4j")
}


// Read the database url (which must included the username/password) from the environment so as not to necessarily
// include them in the command line string
// e.g. jdbc:postgresql://host:port/database?user=userName&password=pass
val dbUrl: String? = System.getenv("LIQUIBASE_DB_URL")
val dbUsername: String? = System.getenv("LIQUIBASE_DB_USERNAME")
val dbPassword: String? = System.getenv("LIQUIBASE_DB_PASSWORD")

// Read any liquibase changelog contexts that should be run from the gradle command line
// e.g. gradle -Pliquibase.contexts="testdata" liquibaseUpdate
val liquibaseContexts: String = findProperty("liquibase.contexts")?.toString().takeIf { !it.isNullOrBlank() } ?: "none"

configure<LiquibaseExtension> {
    activities {
        "main" {
            hashMapOf(
                "logLevel" to "info",
                "changeLogFile" to "database/liquibase/src/changelog/db.changelog-master.xml",
                "url" to dbUrl,
                "username" to dbUsername,
                "password" to dbPassword,
                "contexts" to liquibaseContexts
            )
        }
    }
}

tasks {
    "validateDbProperties" {
        doLast {
            if (dbUrl.isNullOrBlank()) {
                throw GradleException("DB Url Environment Variable [LIQUIBASE_DB_URL] not set")
            }
            if (dbUsername.isNullOrBlank()) {
                throw GradleException("DB Url Environment Variable [LIQUIBASE_DB_USERNAME] not set")
            }
            if (dbPassword.isNullOrBlank()) {
                throw GradleException("DB Url Environment Variable [LIQUIBASE_DB_PASSWORD] not set")
            }
        }
    }
}

tasks.withType<LiquibaseTask> {
    // Ensure DB connection info is present for all tasks that require connection to database
    if (name != "liquibaseValidate" && name != "liquibaseCalculateCheckSum") {
        dependsOn("validateDbProperties")
    }
}
