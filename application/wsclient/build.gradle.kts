apply {
    plugin("java-library")
}

dependencies {
    implementation(project(":common"))
    implementation(project(":domain"))

    implementation("org.apache.httpcomponents:httpclient")
    implementation("com.fasterxml.jackson.core:jackson-annotations")
}
