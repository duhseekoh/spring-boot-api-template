package com.panderalabs.sbat.wsclient

/**
 * No-op marker class for type-safe scanning by ComponentScan/EntityScan.
 */
interface PackageScanMarker
