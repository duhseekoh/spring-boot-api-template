apply {
    plugin("java-library")
    plugin(GradlePlugins.Kotlin.Jpa.id)
}

dependencies {
    implementation(project(":common"))

    implementation("org.springframework.data:spring-data-jpa")
    implementation("org.hibernate:hibernate-core")
    implementation("javax.validation:validation-api")
    implementation("org.hibernate.javax.persistence:hibernate-jpa-2.1-api")
    implementation("com.fasterxml.jackson.core:jackson-annotations")

    //Need this in AbstractBaseEntity to set audit fields
    implementation("org.springframework.boot:spring-boot-starter-security")

    //JsonSerialize annotation
    implementation("com.fasterxml.jackson.core:jackson-databind")
}
