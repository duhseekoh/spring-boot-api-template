package com.panderalabs.sbat.domain

/**
 * No-op marker class for type-safe scanning by ComponentScan/EntityScan.
 */
interface PackageScanMarker
