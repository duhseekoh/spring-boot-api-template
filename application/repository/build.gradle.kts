import com.panderalabs.gradle.kotlin.dsl.api

apply {
    plugin("java-library")
}

dependencies {
    implementation(project(":common"))
    api(project(":domain"))

    implementation("org.springframework:spring-context")
    implementation("org.springframework:spring-tx")
    implementation("org.springframework.data:spring-data-commons")
    implementation("org.springframework.data:spring-data-jpa")
}
