package com.panderalabs.sbat.repository

/**
 * No-op marker class for type-safe scanning by ComponentScan/EnableJpaRepositories.
 */
interface PackageScanMarker
