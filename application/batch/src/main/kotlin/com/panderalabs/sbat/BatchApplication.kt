package com.panderalabs.sbat

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@EnableAutoConfiguration
@EnableBatchProcessing
@ComponentScan(basePackages = ["com.panderalabs.sbat"])
class BatchApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    // System.exit is common for Batch applications since the exit code can be used to drive a workflow
    System.exit(SpringApplication.exit(SpringApplication.run(BatchApplication::class.java, *args)))
}
