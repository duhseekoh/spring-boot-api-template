package com.panderalabs.sbat.web.aop

import mu.KotlinLogging
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component
import org.springframework.util.StopWatch

/**
 * This class will automatically log the invocation and execution time of all classes in the com.ggp.api.[common|controller|service|repository] packages.
 * It is enabled by setting the property spring.aop.auto=true - which should not be done in production. (Just kidding it is on in prod for the launch so we
 * can tune it down to exactly what we want to see).
 */
@Component
@Aspect
class MethodLoggingAspect {

    private val log = KotlinLogging.logger {}

    @Pointcut("execution(* com.parallon.him.outcomes.controller.*.*(..))")
    private fun allControllers() {
    }

    @Pointcut("execution(* com.parallon.him.outcomes.service.*.*(..))")
    private fun allServices() {
    }

    @Pointcut("execution(* com.parallon.him.outcomes.repository.*.*(..))")
    private fun allRepositories() {
    }

    @Pointcut("execution(* com.parallon.him.outcomes.common.*.*(..))")
    private fun allCommon() {
    }


    @Around("allControllers() || allServices() || allRepositories() || allCommon()")
    @Throws(Throwable::class)
    fun logTimeMethod(joinPoint: ProceedingJoinPoint): Any {

        val stopWatch = StopWatch()
        stopWatch.start()

        val retVal = joinPoint.proceed()

        stopWatch.stop()

        val logMessage = StringBuilder()
        logMessage.append(joinPoint.target.javaClass.simpleName)
        logMessage.append(".")
        logMessage.append(joinPoint.signature.name)
        logMessage.append("(")
        // append args
        val args = joinPoint.args
        for (arg in args) {
            logMessage.append(arg).append(",")
        }
        if (args.isNotEmpty()) {
            logMessage.deleteCharAt(logMessage.length - 1)
        }

        logMessage.append(")")
        logMessage.append(" execution time: ")
        logMessage.append(stopWatch.totalTimeMillis)
        logMessage.append(" ms")
        log.trace(logMessage.toString())
        return retVal
    }
}
