package com.panderalabs.sbat.web.controller

import com.panderalabs.exception.BadRequestException
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
internal class HomeController : AbstractBaseController() {

    private val log = KotlinLogging.logger {}

    @GetMapping(path = ["home"], produces = ["application/json"])
    fun foo(): String {
        log.warn { "Welcome to SBAT" }
        return "{\"Message\":\"Welcome to SBAT\"}"
    }

    @GetMapping(path = ["err"], produces = ["application/json"])
    fun err(): String {
        throw BadRequestException("no way jose")
    }
}
