package com.panderalabs.sbat

import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.transaction.annotation.EnableTransactionManagement

@SpringBootApplication
@EnableTransactionManagement
@EnableConfigurationProperties
@ComponentScan(
    basePackageClasses = [com.panderalabs.sbat.domain.PackageScanMarker::class,
        com.panderalabs.sbat.repository.PackageScanMarker::class,
        com.panderalabs.sbat.wsclient.PackageScanMarker::class,
        com.panderalabs.sbat.service.PackageScanMarker::class,
        com.panderalabs.sbat.web.PackageScanMarker::class]
)
class Application : SpringBootServletInitializer() {

    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder =
        application.sources(Application::class.java)

    @Bean
    internal fun kotlinModule(): Module = KotlinModule()
}

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
