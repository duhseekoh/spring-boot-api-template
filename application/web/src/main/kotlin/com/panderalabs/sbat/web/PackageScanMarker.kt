package com.panderalabs.sbat.web

/**
 * No-op marker class for type-safe scanning by ComponentScan/EntityScan.
 */
interface PackageScanMarker
