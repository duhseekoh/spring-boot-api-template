package com.panderalabs.sbat.web.filter

import org.slf4j.MDC
import org.springframework.beans.factory.InitializingBean
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import java.util.*
import javax.servlet.*
import javax.servlet.http.HttpServletResponse

/**
 * Filter to define a unique request identifier for the ServletRequest. If the servlet is for HTTP,
 * a response header will be added as well.
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
class UniqueRequestIdFilter : Filter, InitializingBean {

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        try {
            val requestId = UUID.randomUUID().toString()

            MDC.put("requestId", requestId)

            when (response) {
                is HttpServletResponse -> response.addHeader("api-request-id", requestId)
            }

            chain.doFilter(request, response)
        } finally {
            MDC.remove("requestId")
        }
    }

    override fun destroy() {
    }

    override fun afterPropertiesSet() {
    }

    override fun init(filterConfig: FilterConfig) {
    }
}
