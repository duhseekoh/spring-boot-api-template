package com.panderalabs.sbat.web.security

import org.springframework.security.core.AuthenticationException

/**
 * Base class for JWT exceptions that extends spring security's Authentication exception
 */
open class JwtException(msg: String) : AuthenticationException(msg)

class JwtExpiredException(msg: String) : JwtException(msg)
class JwtMalformedTokenException(msg: String) : JwtException(msg)
class JwtMissingTokenException(msg: String) : JwtException(msg)
