package com.panderalabs.sbat.web.filter

import mu.KotlinLogging
import org.springframework.web.filter.AbstractRequestLoggingFilter
import javax.servlet.http.HttpServletRequest

class Slf4jRequestLoggingFilter : AbstractRequestLoggingFilter() {

    private val log = KotlinLogging.logger {}

    override fun beforeRequest(request: HttpServletRequest, message: String) {
        log.trace { message }
    }

    override fun afterRequest(request: HttpServletRequest, message: String) {
        // NoOp
    }
}

