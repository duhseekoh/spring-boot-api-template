package com.panderalabs.sbat.web.security

import mu.KotlinLogging
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy

/**
 * Local configuration to disable security ONLY when running in Local profile with jwt.disabled = true
 */
@ConditionalOnProperty(value = ["jwt.disabled"], havingValue = "true")
@Profile("local")
@Configuration
class LocalSpringSecurityConfig : WebSecurityConfigurerAdapter() {

    private val log = KotlinLogging.logger {}

    override fun configure(httpSecurity: HttpSecurity) {
        log.error("\n\n\n")
        log.error("******************************************************************************************************************")
        log.error("*                                                                                                                *")
        log.error("*                        RUNNING IN LOCAL MODE WITH JWT SECURITY DISABLED                                        *")
        log.error("*                                                                                                                *")
        log.error("******************************************************************************************************************\n\n\n")
        httpSecurity.authorizeRequests().anyRequest().permitAll().and().sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }
}
