package com.panderalabs.sbat.web.controller

import com.panderalabs.exception.BadRequestException
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder
import java.net.URI

internal abstract class AbstractBaseController {

    @Value("\${application-constants.max_page_size:1000}")
    @Suppress("MagicNumber")
    private var maxPageSize: Long = 1000

    protected fun validatePageable(pageable: Pageable) {
        if (pageable.pageSize > maxPageSize) {
            throw BadRequestException("Page size request parameter exceeded limit of " + maxPageSize)
        }
    }

    /**
     * Returns the URI to a resource, e.g. (GET /resourceName/{id}) by using using the MvcUriComponentsBuilder. By default this  expects the resource's
     * controller method to be named "getOne", though that can be configured by providing the method name
     *
     */
    protected fun getUrl(methodName: String = "getOne", vararg ids: String): URI =
        MvcUriComponentsBuilder.fromMethodName(this.javaClass, methodName, ids).build().toUri()
}


