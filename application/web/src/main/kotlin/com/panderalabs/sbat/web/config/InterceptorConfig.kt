package com.panderalabs.sbat.web.config

import com.panderalabs.sbat.web.config.properties.HttpCachingProperties
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.CacheControl
import org.springframework.web.filter.ShallowEtagHeaderFilter
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.mvc.WebContentInterceptor
import java.util.concurrent.TimeUnit
import javax.servlet.DispatcherType


@Configuration
class InterceptorConfig(val cachingProperties: HttpCachingProperties) : WebMvcConfigurer {


    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(cacheControlInterceptor()).addPathPatterns("/**")
    }

    @Bean
    fun shallowEtagHeaderFilterRegistrationBean(): FilterRegistrationBean<ShallowEtagHeaderFilter> {
        val registration = FilterRegistrationBean<ShallowEtagHeaderFilter>()
        registration.setDispatcherTypes(DispatcherType.REQUEST)
        registration.filter = ShallowEtagHeaderFilter()
        registration.addUrlPatterns("/*")
        return registration
    }

    private fun cacheControlInterceptor(): WebContentInterceptor {
        val interceptor = WebContentInterceptor()
        // Set Cache-Control header to require revalidating asset with the server (hopefully using ETags) after every 5 minutes
        interceptor.cacheControl =
                CacheControl.maxAge(cachingProperties.maxAge, TimeUnit.MINUTES)
                    .mustRevalidate()
                    .staleWhileRevalidate(cachingProperties.staleWhileRevalidate, TimeUnit.MINUTES)
                    .staleIfError(cachingProperties.staleIfError, TimeUnit.MINUTES)
        return interceptor
    }

}
