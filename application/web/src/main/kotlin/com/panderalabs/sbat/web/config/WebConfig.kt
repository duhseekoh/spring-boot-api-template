package com.panderalabs.sbat.web.config

import com.panderalabs.sbat.web.config.properties.RequestLoggingProperties
import com.panderalabs.sbat.web.filter.Slf4jRequestLoggingFilter
import com.panderalabs.sbat.web.filter.UniqueRequestIdFilter
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
class WebConfig(val requestLoggingProperties: RequestLoggingProperties) {

    @Value("\${application-constants.cors.allowed-domains}")
    var corsAllowedDomains: Array<String> = arrayOf("localhost")

    @Bean
    @Suppress("SpreadOperator")
    fun corsConfigurer(): WebMvcConfigurer = object : WebMvcConfigurer {
        override fun addCorsMappings(registry: CorsRegistry) {
            registry.addMapping("/**")
                .allowedOrigins(*corsAllowedDomains)
                .allowedMethods("GET", "PUT", "POST", "DELETE", "HEAD", "OPTIONS")
                //Allow browser to read Location/Content-Type headers
                .exposedHeaders("Location", "Content-Type")
        }
    }

    @ConditionalOnProperty("request.logging.enabled", havingValue = "true")
    @Bean
    fun requestLoggingFilter(): Slf4jRequestLoggingFilter = Slf4jRequestLoggingFilter().apply {
        setIncludeHeaders(requestLoggingProperties.includeHeaders)
        setIncludeClientInfo(requestLoggingProperties.includeClient)
        setIncludePayload(requestLoggingProperties.includeBody)
        setIncludeQueryString(requestLoggingProperties.includeQueryString)
    }

    @Bean
    fun uniqueRequestIdFilter(): UniqueRequestIdFilter = UniqueRequestIdFilter()
}
