package com.panderalabs.sbat.web.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated

/**
 * Control logging of http requests
 */
@ConfigurationProperties(prefix = "request.logging")
@Validated
@Component
@Suppress("MagicNumber")
data class RequestLoggingProperties(
    var enabled: Boolean = true, var includeClient: Boolean = false, var includeQueryString: Boolean = false,
    var includeHeaders: Boolean = false, var includeBody: Boolean = false
)

/**
 * Control HTTP Caching Directive headers
 */
@ConfigurationProperties(prefix = "application-constants.http.caching")
@Component
@Validated
@Suppress("MagicNumber")
data class HttpCachingProperties(
    var maxAge: Long = 5,
    var staleWhileRevalidate: Long = 2,
    var staleIfError: Long = 1440
)
