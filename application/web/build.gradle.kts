import com.gorylenko.GitPropertiesPluginExtension
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.springframework.boot.gradle.dsl.SpringBootExtension
import org.springframework.boot.gradle.tasks.bundling.BootJar
import org.springframework.boot.gradle.tasks.run.BootRun

apply {
    plugin("org.springframework.boot")
    plugin("com.gorylenko.gradle-git-properties")
}

val PROD_PROFILE = "prod"

// For some reason applying the boot plugin in this file vs the parent causes the configuration dsl to not be
// registered in time and the build to fail, thus we must use this syntax
configure<SpringBootExtension> {
    buildInfo()
}

val version: String  by project

tasks {
    "bootRun"(BootRun::class) {
        extra["activeProfiles"] = PROD_PROFILE

        doFirst {
            jvmArgs = listOf("-Dspring.profiles.active=${extra["activeProfiles"]}")
        }
    }
    "bootJar"(BootJar::class) {
        launchScript()
        archiveName = "sbat-web"
        version = version
    }
    "processResources"(ProcessResources::class) {
        filesMatching("**/application.yaml") {
            expand(project.properties)
        }
    }
    "ci" {
        description = """Set the spring.profiles.active to "ci"."""
        group = "Application"
        doFirst {
            tasks["bootRun"].extra["activeProfiles"] = "ci"
        }
    }
    "local" {
        description = """Set the spring.profiles.active to "local"."""
        group = "Application"
        doFirst {
            tasks["bootRun"].extra["activeProfiles"] = "local"
        }
    }
}

configure<GitPropertiesPluginExtension> {
    dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    dateFormatTimeZone = "UTC"
}

// Ensure we don't ever include nasty commons-logging on our classpath
configurations.all({
    exclude(group = "commons-logging", module = "commons-logging")
})


// TODO: Determine how to add a net new sourceset for 'src/integration/kotlin'
//val testImplementation by configurations
//val testRuntimeOnly by configurations
//val integrationImplementation by configurations.creating
//val integrationRuntimeOnly by configurations.creating
//integrationImplementation.extendsFrom(testImplementation)
//integrationRuntimeOnly.extendsFrom(testRuntimeOnly)
// TODO: Determine how to add a net new sourceset for 'src/integration/kotlin'
// TODO: add task for integrationTest to run the tests


dependencies {

    implementation(project(":common"))
    implementation(project(":domain"))
    implementation(project(":repository"))
    implementation(project(":service"))
    implementation(project(":liquibase"))
    implementation(project(":wsclient"))

    implementation("com.panderalabs:exceptions")
    implementation("com.panderalabs:exception-handler-autoconfig")

    implementation("net.logstash.logback:logstash-logback-encoder")
    implementation("org.springframework.boot:spring-boot-starter-logging")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.boot:spring-boot-configuration-processor")
    implementation("org.springframework.boot:spring-boot-actuator")
    implementation("org.springframework.boot:spring-boot-starter-json")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.security.oauth:spring-security-oauth2")

    // In-Memory Database
    runtimeOnly("com.h2database:h2")

    // TODO: More integration config things
    // Add all source & test dependencies to integration test execution
    // integrationImplementation(java.sourceSets["main"].output)
    // integrationImplementation(java.sourceSets["test"].output)

    // TODO: Unsure how to configure this so that the full classpath is available to run integration tests
    //integrationImplementation(java.sourceSets["main"].compileClasspath)
    //integrationImplementation(java.sourceSets["test"].compileClasspath)
    // integrationImplementation("io.rest-assured:rest-assured")
    // integrationRuntimeOnly("com.h2database:h2")

    testImplementation("org.springframework.boot:spring-boot-starter-test")

    // TODO: how the hell do we handle "optional" considering gradle still doesn't have a good solution for this
    // optional("org.springframework.boot:spring-boot-devtools")
}

