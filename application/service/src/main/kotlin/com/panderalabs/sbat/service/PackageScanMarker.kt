package com.panderalabs.sbat.service

/**
 * No-op marker class for type-safe scanning by ComponentScan/EntityScan.
 */
interface PackageScanMarker
