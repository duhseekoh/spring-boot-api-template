import com.panderalabs.gradle.kotlin.dsl.api

apply {
    plugin("java-library")
}

dependencies {
    implementation(project(":common"))
    implementation(project(":repository"))
    implementation(project(":wsclient"))
    api(project(":domain"))

    implementation("javax.transaction:javax.transaction-api")
}
