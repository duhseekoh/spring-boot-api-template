apply {
    plugin("java-library")
}

dependencies {
    implementation("org.slf4j:slf4j-api")
    implementation("io.github.microutils:kotlin-logging")
    testImplementation("ch.qos.logback:logback-classic")
}
