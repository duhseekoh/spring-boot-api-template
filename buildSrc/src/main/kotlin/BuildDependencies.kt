import version.kotlinVersion
import version.springBootVersion

object GradlePlugins {
    object BuiltIn {
        const val java = "java"
        const val javaLibrary = "java-library"
        const val idea = "idea"
        const val jacoco = "jacoco"
        const val mavenPublish = "maven-publish"
    }

    object Kotlin {
        object Jvm {
            const val id = "org.jetbrains.kotlin.jvm"
            const val version = kotlinVersion
        }

        object Spring {
            const val id = "org.jetbrains.kotlin.plugin.spring"
            const val version = kotlinVersion
        }

        object Jpa {
            const val id = "org.jetbrains.kotlin.plugin.jpa"
            const val version = kotlinVersion
        }

        object Kapt {
            const val id = "org.jetbrains.kotlin.kapt"
            const val version = kotlinVersion
        }
    }

    object Nebula {
        object Info {
            const val id = "nebula.info-dependencies"
            const val version = "3.7.1"
        }

        object Publishing {
            const val id = "nebula.maven-base-publish"
            const val version = "7.1.0"
        }
    }

    object SpringBoot {
        const val id = "org.springframework.boot"
        const val version = springBootVersion

        object DependencyManagement {
            const val id = "io.spring.dependency-management"
            const val version = "1.0.5.RELEASE"
        }
    }

    object Versions {
        const val id = "com.github.ben-manes.versions"
        const val version = "0.17.0"
    }

    object Git {
        const val id = "com.gorylenko.gradle-git-properties"
        const val version = "1.4.21"
    }

    object DependencyCheck {
        const val id = "org.owasp.dependencycheck"
        const val version = "3.2.1"
    }

    object Detekt {
        const val id = "io.gitlab.arturbosch.detekt"
        const val version = "1.0.0.RC7-2"
    }

    object Dokka {
        const val id = "org.jetbrains.dokka"
        const val version = "0.9.17"
    }

    object SonarQube {
        const val id = "org.sonarqube"
        const val version = "2.6.2"
    }

    object Liquibase {
        const val id = "org.liquibase.gradle"
        const val version = "1.2.4"
    }
}
