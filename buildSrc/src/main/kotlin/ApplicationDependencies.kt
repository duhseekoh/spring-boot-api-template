import version.springBootVersion

object Libs {

    object Spring {
        object BootDependencies {
            const val coord = "org.springframework.boot:spring-boot-dependencies:$springBootVersion"
        }

        object SecurityOauth {
            const val coord = "org.springframework.security.oauth:spring-security-oauth2:2.3.3.RELEASE"
        }
    }

    object KotlinLogging {
        const val coord = "io.github.microutils:kotlin-logging:1.5.4"
    }

    object RestAssured {
        const val coord = "io.rest-assured:rest-assured:3.1.0"
    }

    object JpaApi {
        const val coord = "org.hibernate.javax.persistence:hibernate-jpa-2.1-api:1.0.2.Final"
    }

    object PanderaCommon {
        const val version = "0.3.0"

        object Exceptions {
            const val coord = "com.panderalabs:exceptions:$version"
        }

        object Extensions {
            const val coord = "com.panderalabs:extensions:$version"
        }

        object ExceptionHandlerAutoconfig {
            const val coord = "com.panderalabs:exception-handler-autoconfig:$version"
        }
    }

    object Spek {
        // Do not "upgrade" this to 1.1.19: https://github.com/spekframework/spek/issues/170
        const val version = "1.1.5"
        const val coord = "org.jetbrains.spek:spek-api:$version"

        object JunitPlatform {
            const val coord = "org.jetbrains.spek:spek-junit-platform-engine:$version"
        }
    }

    object Junit {
        object Platform {
            const val coord = "org.junit.platform:junit-platform-engine:1.2.0"
        }
    }

    object Liquibase {
        object Slf4j {
            const val coord = "com.mattbertolini:liquibase-slf4j:2.0.0"
        }
    }

    object Logstash {
        object LogstashLogbackEncoder {
            const val coord = "net.logstash.logback:logstash-logback-encoder:5.2"
        }
    }
}
