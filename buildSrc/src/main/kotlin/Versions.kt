package version

// Defined here as these versions are used by both Application Dependencies and Gradle Plugins
const val springBootVersion = "2.0.4.RELEASE"
const val kotlinVersion = "1.2.61"
