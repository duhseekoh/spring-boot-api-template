@file:Suppress("TooManyFunctions", "LongParameterList", "WildcardImport" )
package com.panderalabs.gradle.kotlin.dsl

/**
 * Cribbed from Kotlin-DSL accessors.kt to add extension functions for adding dependencies to the 'api'
 * configuration defined by the 'java-library' plugin as this is not handled by default by the kotlin-dsl.
 */

import org.gradle.api.artifacts.*
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.kotlin.dsl.add
import org.gradle.kotlin.dsl.create

val ConfigurationContainer.`api`: Configuration
    get() = getByName("api")

/**
 * Adds a dependency to the 'api' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.`api`(dependencyNotation: Any): Dependency? =
    add("api", dependencyNotation)

/**
 * Adds a dependency to the 'api' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
inline
fun DependencyHandler.`api`(
    dependencyNotation: String,
    dependencyConfiguration: ExternalModuleDependency.() -> Unit
): ExternalModuleDependency =
    add("api", dependencyNotation, dependencyConfiguration)

/**
 * Adds a dependency to the 'api' configuration.
 *
 * @param group the group of the module to be added as a dependency.
 * @param name the name of the module to be added as a dependency.
 * @param version the optional version of the module to be added as a dependency.
 * @param configuration the optional configuration of the module to be added as a dependency.
 * @param classifier the optional classifier of the module artifact to be added as a dependency.
 * @param ext the optional extension of the module artifact to be added as a dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.`api`(
    group: String,
    name: String,
    version: String? = null,
    configuration: String? = null,
    classifier: String? = null,
    ext: String? = null
): ExternalModuleDependency =
    create(group, name, version, configuration, classifier, ext).apply { add("api", this) }

/**
 * Adds a dependency to the 'api' configuration.
 *
 * @param group the group of the module to be added as a dependency.
 * @param name the name of the module to be added as a dependency.
 * @param version the optional version of the module to be added as a dependency.
 * @param configuration the optional configuration of the module to be added as a dependency.
 * @param classifier the optional classifier of the module artifact to be added as a dependency.
 * @param ext the optional extension of the module artifact to be added as a dependency.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.create]
 * @see [DependencyHandler.add]
 */
inline
fun DependencyHandler.`api`(
    group: String,
    name: String,
    version: String? = null,
    configuration: String? = null,
    classifier: String? = null,
    ext: String? = null,
    dependencyConfiguration: ExternalModuleDependency.() -> Unit
): ExternalModuleDependency =
    add("api", create(group, name, version, configuration, classifier, ext), dependencyConfiguration)

/**
 * Adds a dependency to the 'api' configuration.
 *
 * @param dependency dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
inline
fun <T : ModuleDependency> DependencyHandler.`api`(dependency: T, dependencyConfiguration: T.() -> Unit): T =
    add("api", dependency, dependencyConfiguration)

val ConfigurationContainer.`integrationImplementation`: Configuration
    get() = getByName("integrationImplementation")

/**
 * Adds a dependency to the 'integrationImplementation' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.`integrationImplementation`(dependencyNotation: Any): Dependency? =
    add("integrationImplementation", dependencyNotation)

/**
 * Adds a dependency to the 'integrationImplementation' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
inline
fun DependencyHandler.`integrationImplementation`(
    dependencyNotation: String,
    dependencyConfiguration: ExternalModuleDependency.() -> Unit
): ExternalModuleDependency =
    add("integrationImplementation", dependencyNotation, dependencyConfiguration)

/**
 * Adds a dependency to the 'integrationImplementation' configuration.
 *
 * @param group the group of the module to be added as a dependency.
 * @param name the name of the module to be added as a dependency.
 * @param version the optional version of the module to be added as a dependency.
 * @param configuration the optional configuration of the module to be added as a dependency.
 * @param classifier the optional classifier of the module artifact to be added as a dependency.
 * @param ext the optional extension of the module artifact to be added as a dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.`integrationImplementation`(
    group: String,
    name: String,
    version: String? = null,
    configuration: String? = null,
    classifier: String? = null,
    ext: String? = null
): ExternalModuleDependency =
    create(group, name, version, configuration, classifier, ext).apply { add("integrationImplementation", this) }

/**
 * Adds a dependency to the 'integrationImplementation' configuration.
 *
 * @param group the group of the module to be added as a dependency.
 * @param name the name of the module to be added as a dependency.
 * @param version the optional version of the module to be added as a dependency.
 * @param configuration the optional configuration of the module to be added as a dependency.
 * @param classifier the optional classifier of the module artifact to be added as a dependency.
 * @param ext the optional extension of the module artifact to be added as a dependency.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.create]
 * @see [DependencyHandler.add]
 */
inline
fun DependencyHandler.`integrationImplementation`(
    group: String,
    name: String,
    version: String? = null,
    configuration: String? = null,
    classifier: String? = null,
    ext: String? = null,
    dependencyConfiguration: ExternalModuleDependency.() -> Unit
): ExternalModuleDependency =
    add("integrationImplementation", create(group, name, version, configuration, classifier, ext), dependencyConfiguration)

/**
 * Adds a dependency to the 'integrationImplementation' configuration.
 *
 * @param dependency dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
inline
fun <T : ModuleDependency> DependencyHandler.`integrationImplementation`(dependency: T, dependencyConfiguration: T.() -> Unit): T =
    add("integrationImplementation", dependency, dependencyConfiguration)

val ConfigurationContainer.`integrationRuntimeOnly`: Configuration
    get() = getByName("integrationRuntimeOnly")

/**
 * Adds a dependency to the 'integrationRuntimeOnly' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.`integrationRuntimeOnly`(dependencyNotation: Any): Dependency? =
    add("integrationRuntimeOnly", dependencyNotation)

/**
 * Adds a dependency to the 'integrationRuntimeOnly' configuration.
 *
 * @param dependencyNotation notation for the dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
inline
fun DependencyHandler.`integrationRuntimeOnly`(
    dependencyNotation: String,
    dependencyConfiguration: ExternalModuleDependency.() -> Unit
): ExternalModuleDependency =
    add("integrationRuntimeOnly", dependencyNotation, dependencyConfiguration)

/**
 * Adds a dependency to the 'integrationRuntimeOnly' configuration.
 *
 * @param group the group of the module to be added as a dependency.
 * @param name the name of the module to be added as a dependency.
 * @param version the optional version of the module to be added as a dependency.
 * @param configuration the optional configuration of the module to be added as a dependency.
 * @param classifier the optional classifier of the module artifact to be added as a dependency.
 * @param ext the optional extension of the module artifact to be added as a dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
fun DependencyHandler.`integrationRuntimeOnly`(
    group: String,
    name: String,
    version: String? = null,
    configuration: String? = null,
    classifier: String? = null,
    ext: String? = null
): ExternalModuleDependency =
    create(group, name, version, configuration, classifier, ext).apply { add("integrationRuntimeOnly", this) }

/**
 * Adds a dependency to the 'integrationRuntimeOnly' configuration.
 *
 * @param group the group of the module to be added as a dependency.
 * @param name the name of the module to be added as a dependency.
 * @param version the optional version of the module to be added as a dependency.
 * @param configuration the optional configuration of the module to be added as a dependency.
 * @param classifier the optional classifier of the module artifact to be added as a dependency.
 * @param ext the optional extension of the module artifact to be added as a dependency.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.create]
 * @see [DependencyHandler.add]
 */
inline
fun DependencyHandler.`integrationRuntimeOnly`(
    group: String,
    name: String,
    version: String? = null,
    configuration: String? = null,
    classifier: String? = null,
    ext: String? = null,
    dependencyConfiguration: ExternalModuleDependency.() -> Unit
): ExternalModuleDependency =
    add("integrationRuntimeOnly", create(group, name, version, configuration, classifier, ext), dependencyConfiguration)

/**
 * Adds a dependency to the 'integrationRuntimeOnly' configuration.
 *
 * @param dependency dependency to be added.
 * @param dependencyConfiguration expression to use to configure the dependency.
 * @return The dependency.
 *
 * @see [DependencyHandler.add]
 */
inline
fun <T : ModuleDependency> DependencyHandler.`integrationRuntimeOnly`(dependency: T, dependencyConfiguration: T.() -> Unit): T =
    add("integrationRuntimeOnly", dependency, dependencyConfiguration)
