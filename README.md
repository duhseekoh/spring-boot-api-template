# Spring Boot Application Template (SBAT)

This is an opinionated template for creating new Spring Boot Kotlin projects.

## Technologies

SBAT uses (at least) the following technologies:

Capability               | Tool/Framework                                                                                                           | Version
-------------------------|--------------------------------------------------------------------------------------------------------------------------|--------
Application Language     | [Kotlin](http://www.kotlinlang.org)                                                                                      | 1.2
Development Framework    | [Spring](http://projects.spring.io/spring-framework/)                                                                    | 5.x
Application Framework    | [Spring Boot](http://projects.spring.io/spring-boot/)                                                                    | 2.x
Security                 | [Spring Security](http://projects.spring.io/spring-security/)                                                            | 5.x
Unit Tests               | [Spek](http://spekframework.org/)                                                                                        | 1.1.x
Integration Tests        | [Rest Assured](https://code.google.com/p/rest-assured/)                                                                  | 3.1.x
Dependency Management    | [Gradle](https://gradle.org/)                                                                                            | 4.7
Dev App Server           | [Embedded Tomcat](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-embedded-container) | 8.x
Relational Data Store    | [H2](https://h2database.com)                                                                                             | 1.4.x
Relational Database Mgmt | [Liquibase](https://www.liquibase.org)                                                                                   | 3.6.x

## Overview

SBAT defines a template application structure for a multi-module gradle project. Multi-module is important because it supports binary code reusability and enforces proper application layering visibility constraints (e.g. code in the `repository` module cannot call methods on code in the `service` module).

The template defines the structure as follows:

```
.
├── application/
│   ├── batch/
│   │   └── build.gradle.kts
│   ├── common/
│   │   └── build.gradle.kts
│   ├── domain/
│   │   └── build.gradle.kts
│   ├── repository/
│   │   └── build.gradle.kts
│   ├── service/
│   │   └── build.gradle.kts
│   ├── web/
│   │   └── build.gradle.kts
│   └── wsclient/
│       └── build.gradle.kts
├── build.gradle.kts
├── buildSrc/
├── database/
│   └── liquibase/
│       └── build.gradle.kts
├── docs/
└── serviceclient/
    ├── client/
    │   └── build.gradle.kts
    └── stub/
        └── build.gradle.kts        
```

At the root of the project is the parent `build.gradle.kts` as well as the following directories:
* [buildSrc](buildSrc)
  * Contains custom gradle functionality used during build process
* [application](application)
  * Contains all modules for application source code
* [database](database)
  * Contains modules for managing data and/or structure for document / relational databases
* [serviceclient](serviceclient)
  * Standalone module that provides a java SDK and Stub implementation for this service
* [docs](docs)
  * Contains any relevant documentation for the application

## Dependencies Management

The parent `build.gradle.kts` in the root directory imports both the Spring Platform BOM and Spring Boot Dependencies to leverage the dependency versions they define. If a required dependency is not already transitively included via those imports (or if the version of a transitively included dependency must be overridden) the version should be externalized as a variable in `gradle.properties` and the dependency declared in the parent `build.gradle.kts`'s `allProjects.dependencyManagement` section. This allows the dependency version to be defined once and then used from any sub module without needing to specify the version again. 

Each of the sub-modules has its own `build.gradle.kts` that should define *all* dependencies used within that project. It is especially important to declare all transitive dependencies for any modules that may be used by other projects. 

## Creating a New Micro Service from Template
* Fork the template into a new repository
* Clone the new repository to your machine
* Remove the folders for any unnecessary modules (e.g. `applications/batch` for a web only app or `database/liquibase` for a app using a document datastore) and update `settings.gradle`
* Replace any references to "sbat" in the application with your project's name
* Update this readme file to be specific to your application
* Add a git remote pointing to the template to your clone
    * `git remote add template https://github.com/panderasystems/spring-boot-api-template.git`
* Periodically update the remote and merge in changes from template to ensure all services are using common structures and approaches
    * `git merge template/master`

## Application.yaml Configuration

All application specific constants should go under the `application-constants:` element

## Spring Profiles
local, ci, dev, staging, production
### Local configuration overloading


## Liquibase 
#### Liquibase Directory Structure
#### Executing Liquibase from Gradle

## Logging with Logback

### Request Logging
document request id, and request logging config params  

## Error Handling
exception types, controller advice

## Base Controller
-- getOne Convention - set the Location header 

## Cors Support
Cors support is included in the app as per http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#_global_cors_configuration
Ensure the proper domains and paths are set in `WebConfig.kt`
 
## Http Caching & ETags
