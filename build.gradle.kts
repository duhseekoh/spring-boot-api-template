import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    // Standard gradle plugins
    id(GradlePlugins.BuiltIn.idea)
    id(GradlePlugins.BuiltIn.jacoco)
    id(GradlePlugins.BuiltIn.mavenPublish)

    // Plugins applied to root project
    id(GradlePlugins.Kotlin.Jvm.id) version (GradlePlugins.Kotlin.Jvm.version)
    id(GradlePlugins.Versions.id) version (GradlePlugins.Versions.version)
    id(GradlePlugins.Detekt.id) version (GradlePlugins.Detekt.version)
    id(GradlePlugins.SpringBoot.DependencyManagement.id) version (GradlePlugins.SpringBoot.DependencyManagement.version)
    id(GradlePlugins.Nebula.Info.id) version (GradlePlugins.Nebula.Info.version)
    id(GradlePlugins.DependencyCheck.id) version (GradlePlugins.DependencyCheck.version)
    id(GradlePlugins.SonarQube.id) version (GradlePlugins.SonarQube.version)

    // Plugins not applied to root, but only to child projects
    id(GradlePlugins.Git.id) version (GradlePlugins.Git.version) apply (false)
    id(GradlePlugins.Dokka.id) version (GradlePlugins.Dokka.version) apply (false)
    id(GradlePlugins.Kotlin.Kapt.id) version (GradlePlugins.Kotlin.Kapt.version) apply (false)
    id(GradlePlugins.Kotlin.Jpa.id) version (GradlePlugins.Kotlin.Jpa.version) apply (false)
    id(GradlePlugins.Kotlin.Spring.id) version (GradlePlugins.Kotlin.Spring.version) apply (false)
    id(GradlePlugins.SpringBoot.id) version (GradlePlugins.SpringBoot.version) apply (false)
    id(GradlePlugins.Liquibase.id) version (GradlePlugins.Liquibase.version) apply (false)
}

detekt {
    //version = PluginVersions.detekt
    profile("main", Action {
        config = "${rootProject.projectDir}/detekt.yml"
        filters = ".*test.*,.*/resources/.*,.*/tmp/.*,.*build.gradle.kts"
        output = "build/reports/detekt"
    })
}

val check: Task by tasks
check.dependsOn("detektCheck")

allprojects {

    apply {
        plugin(GradlePlugins.BuiltIn.idea)
        plugin(GradlePlugins.BuiltIn.java)
        plugin(GradlePlugins.BuiltIn.mavenPublish)
        plugin(GradlePlugins.Versions.id)
        plugin(GradlePlugins.SpringBoot.DependencyManagement.id)
        plugin(GradlePlugins.DependencyCheck.id)
    }

    repositories {
        maven {
            setUrl("https://repository.panderacloud.com/repository/maven-all/")
            credentials {
                username = property("PANDERA_REPO_USER").toString()
                password = property("PANDERA_REPO_PASSWORD").toString()
            }
        }
    }

    idea {
        module {
            isDownloadJavadoc = true
            isDownloadSources = true
        }
    }


    dependencyManagement {
        imports {
            // Import and use the Spring platform dependencies to allow us to only override where necessary
            mavenBom(Libs.Spring.BootDependencies.coord)
        }
        dependencies {
            dependency(Libs.JpaApi.coord)
            dependency(Libs.KotlinLogging.coord)
            dependency(Libs.PanderaCommon.Exceptions.coord)
            dependency(Libs.PanderaCommon.Extensions.coord)
            dependency(Libs.PanderaCommon.ExceptionHandlerAutoconfig.coord)
            dependency(Libs.RestAssured.coord)
            dependency(Libs.Spek.coord)
            dependency(Libs.Spek.JunitPlatform.coord)
            dependency(Libs.Spring.SecurityOauth.coord)
            dependency(Libs.Junit.Platform.coord)
            dependency(Libs.Liquibase.Slf4j.coord)
            dependency(Libs.Logstash.LogstashLogbackEncoder.coord)
        }
    }
}

configure(subprojects.filter {
    it.projectDir.path.contains("/application") || it.projectDir.path.contains("/serviceclient")
}) {

    apply {
        plugin(GradlePlugins.Kotlin.Jvm.id)
        plugin(GradlePlugins.Nebula.Info.id)
        plugin(GradlePlugins.Dokka.id)
        plugin(GradlePlugins.Kotlin.Kapt.id)
        plugin(GradlePlugins.Kotlin.Spring.id)
        plugin(GradlePlugins.DependencyCheck.id)
    }

    java {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    //see https://github.com/gradle/kotlin-dsl/issues/872
    tasks.withType<Test> {
        useJUnitPlatform()
    }

    extra["xLintArg"] = "-Xlint:all,-options,-serial,-processing"

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "1.8"
            javaParameters = true
        }
    }

    tasks {
        val sourcesJar by creating(Jar::class) {
            classifier = "sources"
            from(java.sourceSets.getByName("main").allSource)
        }

        val dokkaJavadoc by creating(org.jetbrains.dokka.gradle.DokkaTask::class) {
            outputFormat = "javadoc"
            outputDirectory = "${buildDir}/dokkaJavadoc"
        }

        val dokkaDoc by creating(org.jetbrains.dokka.gradle.DokkaTask::class) {
            outputFormat = "html"
            outputDirectory = "${buildDir}/dokkaDoc"
        }

        val javadocJar by creating(Jar::class) {
            dependsOn("dokkaJavadoc")
            from("${buildDir}/dokkaJavadoc")
            classifier = "javadoc"
        }

        artifacts {
            add("archives", sourcesJar)
            add("archives", javadocJar)
        }
    }

    dependencies {
        kapt("org.springframework.boot:spring-boot-configuration-processor")

        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
        implementation("org.jetbrains.kotlin:kotlin-reflect")

        testImplementation("org.jetbrains.kotlin:kotlin-test")
        testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

        testImplementation("org.jetbrains.spek:spek-api")
        testRuntimeOnly("org.jetbrains.spek:spek-junit-platform-engine")
        testRuntimeOnly("org.junit.platform:junit-platform-engine")
    }
}


val wrapper: Wrapper by tasks
wrapper.gradleVersion = "4.9"


